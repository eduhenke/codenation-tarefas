from formatter import *

class Country:
    def __init__(self, *args):
        self.code, self.name, self.population, self.area, self.continent = args
    def __str__(self):
        return '{0} : ({1}, {2}, {3}, {4})'.format(*self.attributes)
    def __repr__(self):
        return str(self)
    @property
    def attributes(self):
        return (self.name, self.code, self.continent, self.area, self.population)

    def format(self, width=([0]*5)):    # formata cada campo conforme o tamanho passado
        return '{0} : ({1}, {2}, {3}, {4})'.format(*[spacePad(self.attributes[i], width[i]) for i in range(len(self.attributes))])


class HenkeBase:
    def __init__(self, name):
        self.objects = []
    def __str__(self):
        return formatList(self.objects)
    def add(self, obj):
        self.objects += obj
    def remove(self, obj):
        self.objects.remove(obj)

    def filter(self, **kwargs):
        res = []
        for obj in self.objects:
            filts = kwargs['filters']
            vals = kwargs['values']
            if len(filts)!=len(vals):
                raise ValueError("Length of filters must be the same as the length of the values it will filter for")
            elif all([getattr(obj, filts[i]) == vals[i] for i in range(len(filts))]):
                res.append(obj)
        return res

    def funcFilter(self, func):
        return [obj for obj in self.objects if func(obj)]

    def top(self, num):
        return sorted(self.objects, key=lambda c: c.population, reverse=True)[:num]
