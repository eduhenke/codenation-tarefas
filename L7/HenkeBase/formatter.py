def spacePad(val, width):
    return str(val)+(' '*(width-len(str(val))))

def formatList(objList): # nao tente entender, simplesmente deixa bonito. to avisando, mas se quiser continuar...
    objAttrLen = len(objList[0].attributes)     # essa funcao formata uma lista de objetos conforme
    maxLen = [0]*objAttrLen                     # o tamanho maximo de cada atributo a partir dessa lista
    for obj in objList:
        for i in range(objAttrLen):
            l = len(str(obj.attributes[i]))
            if l > maxLen[i]:
                maxLen[i] = l
    res = "".join([spacePad(['Name','    Code ','Continent ','Pop. ','  Area'][i], maxLen[i]) for i in range(objAttrLen)])+'\n'
    for obj in objList:
        res += obj.format(maxLen)+'\n'
    return res
