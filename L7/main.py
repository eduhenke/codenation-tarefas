# encoding=utf8
import sys, requests
from HenkeBase.database import *
reload(sys)
sys.setdefaultencoding('utf8')

r = requests.get('https://restcountries.eu/rest/v2/all')
cDict = r.json() # dictionary
countries = [Country(c['alpha3Code'],c['name'],c['population'],c['area'],c['region']) for c in cDict]

db = HenkeBase('Countries')
db.add(countries)

#print formatList(db.filter(filters=['continent'], values=['Europe']))
print formatList(db.funcFilter(lambda c: c.area>8000000 and c.continent == 'Americas'))
print db
print formatList(db.top(10))
