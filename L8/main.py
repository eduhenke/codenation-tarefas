import random as rnd
from functools import wraps

def file_to_list(file):
    return file.read().split()

def gen_text(words):
    for i in range(10):
        yield rnd.choice(words)

def all_files(path, read_or_write='r'):
    for i in range(5000):
        with open('{0}/{1}.txt'.format(path,i),read_or_write) as f:
            yield f

def gen_all_texts(words, path):
    for f in all_files(path,'w'):
        for word in gen_text(words):
            f.write(word+' ')

def files_that_have(word, path):
    for f in all_files(path):
        file_words = file_to_list(f)
        if word in file_words:
            yield f.name

words = file_to_list(open('words3.txt'))
#gen_all_texts(words, 'txts')
# for file_name in files_that_have('forward','txts'):
#    print file_name
